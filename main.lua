function love.load()
    x0 = 400
    y0 = 300
    x1 = 50
    x2 = 750
    y1 = 250
    y2 = 250
    v = 500
    vx = 500
    vy = 500
    w = 24
    r = 12
    l = 120
    score1 = 0
    score2 = 0
    roundEnd = false
    font = love.graphics.newFont( "8bit.ttf" , 48)
    text1 = love.graphics.newText( font, score1 )
    text2 = love.graphics.newText( font, score2)
    source = love.audio.newSource( "bloop.wav", "static" )
end

function love.draw()
    if not roundEnd then
        love.graphics.circle("fill", x0, y0, r )
    end
    love.graphics.rectangle("fill", x1 - w, y1, w, l )
    love.graphics.rectangle("fill", x2, y2, w, l )
    love.graphics.draw(text1, 300, 50)
    love.graphics.draw(text2, 500, 50)
end

function love.update(dt)
    --Left Paddle
    if love.keyboard.isDown("s") then
        y1 = y1 + (v * dt)
    end
    if love.keyboard.isDown("w") then
        y1 = y1 - (v * dt)
    end
    if y1 > 600 - l then y1 = 600 - l end
    if y1 < 0 then y1 = 0 end
    -- Right Paddle
    if love.keyboard.isDown("down") then
        y2 = y2 + (v * dt)
    end
    if love.keyboard.isDown("up") then
        y2 = y2 - (v * dt)
    end
    if y2 > 600 - l then y2 = 600 - l end
    if y2 < 0 then y2 = 0 end
    -- Ball
    x0 = x0 + (vx * dt)
    y0 = y0 + (vy * dt)
    if y0 > 600 - r then
        vy = -vy
        y0 = 600 - r
        if not roundEnd then love.audio.play( source )
        end
    end
    if y0 < r then
        vy = -vy
        y0 = r
        if not roundEnd then love.audio.play( source )
        end
     end
    if x0 < x1 + r and y1 < y0 and y0 < y1 + l then
        vx = -vx
        x0 = x1 + r
        if not roundEnd then love.audio.play( source )
        end
    end
    if x0 > x2 - r and y2 < y0 and y0 < y2 + l then
        vx = -vx
        x0 = x2 - r
        if not roundEnd then love.audio.play( source )
        end
    end
    if x0 > 800 - r and not roundEnd then
        roundEnd = true
        score1 = score1 + 1
        text1:set(score1)
    end
    if x0 < r and not roundEnd then
        roundEnd = true
        score2 = score2 + 1
        text2:set(score2)
    end
    if love.keyboard.isDown("return") then 
        roundEnd = true
        score1 = 0
        text1:set(score1)
        score2 = 0
        text2:set(score2)
    end
    if roundEnd then 
        if love.keyboard.isDown("space") then
            roundEnd = false
            x0 = 400; y0 = 300
        end
    end
end